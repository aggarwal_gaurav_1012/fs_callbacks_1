/*    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');

function operations() {
    const filePath1 = 'newFile1.txt';
    const filePath2 = 'newFile2.txt';
    const filePath3 = 'newFile3.txt';

    // Read the given file lipsum.txt
    fs.readFile('../lipsum_1.txt', 'utf-8', (error, data) => {
        if (error) {
            console.error("Error in reading file");
            return;
        } else {
            //Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
            const uppercaseContent = data.toUpperCase();

            fs.writeFile('newFile1.txt', uppercaseContent, (error) => {
                if (error) {
                    console.log("Error in writing file 1");
                    return;
                } else {
                    console.log("New file 1 written successfully");

                    // Store the name of the new file in filenames.txt
                    fs.appendFile('filenames.txt', filePath1 + '\n', (error) => {
                        if (error) {
                            console.log("Error in appending newFile1.txt to filenames.txt:");
                            return;
                        } else {
                            console.log('newFile1 appended to filenames.txt');
                        }

                        // Read the new file and convert it to lower case.
                        fs.readFile('./newFile1.txt', 'utf-8', (error, data) => {
                            if (error) {
                                console.log("Error in reading newFile1.txt");
                            } else {
                                const lowercaseContent = data.toLowerCase();

                                // Split the contents into sentences.
                                const sentences = lowercaseContent.split('.');
                                const trimmedSentences = sentences.map(sentence => sentence.trim()).join('.\n');

                                // Then write it to a new file.
                                fs.writeFile('newFile2.txt', trimmedSentences, (error) => {
                                    if (error) {
                                        console.log("Error in writing file 2");
                                    } else {
                                        console.log("New File 2 written successfully");

                                        // Store the name of the new file in filenames.txt
                                        fs.appendFile('filenames.txt', filePath2 + '\n', (error) => {
                                            if (error) {
                                                console.log("Error in appending newFile2.txt to filenames.txt:");
                                                return;
                                            } else {
                                                console.log('newFile2 appended to filenames.txt');

                                                // Read the new file
                                                fs.readFile('./newFile2.txt', 'utf-8', (error, data) => {
                                                    if (error) {
                                                        console.log("Error in reading NewFile2.txt")
                                                    } else {

                                                        // Sort the content in the file
                                                        const lines = data.split('\n');
                                                        const sortedLines = lines.sort();
                                                        const sortedContent = sortedLines.join('\n');

                                                        // write it out to a new file.
                                                        fs.writeFile('newFile3.txt', sortedContent, (error) => {
                                                            if (error) {
                                                                console.log("Error in writing in newFile3.txt");
                                                            } else {
                                                                console.log("New File 3 written successfully");

                                                                // Store the name of the new file in filenames.txt
                                                                fs.appendFile('filenames.txt', filePath3 + '\n', (error) => {
                                                                    if (error) {
                                                                        console.log("Error in appending newFile3.txt to filenames.txt");
                                                                    } else {
                                                                        console.log("newFile3 appended to filenames.txt");

                                                                        // Read the contents of filenames.txt
                                                                        fs.readFile('./filenames.txt', 'utf-8', (error, data) => {
                                                                            if (error) {
                                                                                console.log("filenames.txt not found");
                                                                            } else {

                                                                                // Delete all the new files that are mentioned in that list simultaneously.
                                                                                fs.unlink(filePath1, (error) => {
                                                                                    if( error) {
                                                                                        console.log(filePath1 ,"not found");
                                                                                    } else {
                                                                                        console.log(filePath1, "deleted successfully");

                                                                                        fs.unlink(filePath2, (error) => {
                                                                                            if (error) {
                                                                                                console.log(filePath2, "not found");
                                                                                            } else {
                                                                                                console.log(filePath2, "deleted successfully");

                                                                                                fs.unlink(filePath3, (error) => {
                                                                                                    if (error) {
                                                                                                        console.log(filePath3, "not found");
                                                                                                    } else {
                                                                                                        console.log(filePath3, "deleted successfully");
                                                                                                    }
                                                                                                })
                                                                                            }
                                                                                        })
                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    });
                }
            })
        }
    });
}

module.exports = operations;