/* Problem1:
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs');

function makingNewDirectory() {

    fs.mkdir('newDirectory', (error) => {
        if (error) {
            console.log("Directory already exists");
            return;
        } else {
            console.log("Directory created");

            fs.writeFile(`newDirectory/randomFile1.json`, '', (error) => {
                if (error) {
                    console.log(`File 1 already exists`);
                } else {
                    console.log(`File 1 created`);

                    fs.unlink(`newDirectory/randomFile1.json`, (error) => {
                        if (error) {
                            console.log(`File 1 not found`);
                        } else {
                            console.log(`File 1 deleted`);
                        }
                    });

                    fs.writeFile(`newDirectory/randomFile2.json`, '', (error) => {
                        if (error) {
                            console.log(`File 2 already exists`);
                        } else {
                            console.log(`File 2 created`);

                            fs.unlink(`newDirectory/randomFile2.json`, (error) => {
                                if (error) {
                                    console.log(`File 2 not found`);
                                } else {
                                    console.log(`File 2 deleted`);
                                }
                            });

                            fs.writeFile(`newDirectory/randomFile3.json`, '', (error) => {
                                if (error) {
                                    console.log(`File 3 already exists`);
                                } else {
                                    console.log(`File 3 created`);

                                    fs.unlink(`newDirectory/randomFile3.json`, (error) => {
                                        if (error) {
                                            console.log(`File 3 not found`);
                                        } else {
                                            console.log(`File 3 deleted`);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

module.exports = makingNewDirectory;